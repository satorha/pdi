#include "Seccao.h"

Seccao::Seccao()
{
    m_quantidade_pontos = 0;
    impossivel = false;
}

Seccao::~Seccao()
{
    //dtor
}

void rodar(Point& p, float coss)
{
    const double seno = -sin(coss);
    const double cosseno = cos(coss);

    Point atual = p;
    p.x = atual.x*cosseno - atual.y*seno;
    p.y = atual.x*seno + atual.y*cosseno;

}

void Seccao::NormalizaCurva(Point* pontos, int quant, double dimensao_coisa)
{
    Point vet = pontos[quant-1] - pontos[0];

    Point diagonal = Point(dimensao_coisa, 0);

    //normaliza a rotacao
    double angulo = acos(vet.dot(diagonal)/(sqrt(vet.dot(vet))*sqrt(diagonal.dot(diagonal))));

    Point primeiro = pontos[0];
    for(int i=0; i < quant; i++)
    {
        pontos[i] -= primeiro;
    }

    for(int i=0; i < quant; i++)
    {
        rodar(pontos[i], angulo);
    }

    double numero = sqrt(diagonal.dot(diagonal))/sqrt(vet.dot(vet));
    for(int i=0; i < quant; i++)
    {
        pontos[i].x = (pontos[i].x)*numero;
        pontos[i].y = (pontos[i].y)*numero + dimensao_coisa/2;
        if((pontos[i].x >= dimensao_coisa) || (pontos[i].y >= dimensao_coisa))
            impossivel = true;
    }


}

/*
    Desenha uma linha
*/
void bresenham1(int x1, int y1, int x2, int y2, Vetor_bool& imagem, int lado)
{
        int slope;
        int dx, dy, incE, incNE, d, x, y;
        // Onde inverte a linha x1 > x2
        if (x1 > x2){
            bresenham1(x2, y2, x1, y1, imagem, lado);
             return;
        }
        dx = x2 - x1;
        dy = y2 - y1;

        if (dy < 0){
            slope = -1;
            dy = -dy;
        }
        else{
           slope = 1;
        }
        // Constante de Bresenham
        incE = 2 * dy;
        incNE = 2 * dy - 2 * dx;
        d = 2 * dy - dx;
        y = y1;
        for (x = x1; x <= x2; x++){

            if(x < lado && y < lado)
            {
                imagem.set(y*lado + x, true);
            }
            if (d <= 0){
              d += incE;
            }
            else{
              d += incNE;
              y += slope;
            }
        }
  }

void Seccao::GeraMatrizCurva()
{
    Point* pontos = &m_pontos[0];
    const double dimensao_coisa = FATOR_NORMALIZANTE;

    NormalizaCurva(pontos, m_quantidade_pontos, dimensao_coisa);

    for(int camada_atual = 0; camada_atual < QUANT_CAMADAS; camada_atual++)
    {
        Vetor_bool tmp;
        int ordem = RESOLUCAO_CAMADA[camada_atual];

        int _p = dimensao_coisa/ordem;
        tmp.resize(ordem*ordem);

        int anterior_x = pontos[0].x/_p;
        int anterior_y = pontos[0].y/_p;

        for(int a=0; a < m_quantidade_pontos; a++)
        {
            Point p = pontos[a];
            int matrix_y = p.y/_p;
            int matrix_x = p.x/_p;

            if(matrix_x >= ordem || matrix_y >= ordem)
                impossivel = true;


            bresenham1(anterior_x, anterior_y, matrix_x, matrix_y, tmp, ordem);

//
            anterior_x = matrix_x;
            anterior_y = matrix_y;
        }
        m_imagem_2d.push_back(Nodo_imagem(tmp, camada_atual));
    }
}


void Seccao::processar()
{
    GeraMatrizCurva();
}


