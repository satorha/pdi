#include "Vetor_bool.h"
#include <iostream>

Vetor_bool::Vetor_bool()
{
    clear();
}

Vetor_bool::~Vetor_bool()
{

}

void Vetor_bool::clear()
{
    m_dados.clear();
    m_size = 0;
    novo_pedaco();
}

void Vetor_bool::novo_pedaco()
{
    m_dados.push_back(0);
    m_pos_atual = 1;
}

void Vetor_bool::push_back(bool v)
{
    if(m_pos_atual == 0)
    {
        novo_pedaco();
    }

    if(v)
        m_dados.back() |= m_pos_atual;

    m_pos_atual <<= 1;

    m_size++;
}

void Vetor_bool::resize(int s)
{
    clear();
    m_dados.reserve((s/32)+5);
    for(int i=0; i < s; i++)
        push_back(false);
}

bool Vetor_bool::at(int pos) const
{
    const uint32_t& chunk = m_dados[pos/32];

    if(chunk & (1 << (pos%32)))
        return true;

    return false;
}

void Vetor_bool::set(unsigned int pos, bool val)
{
    if(pos >= m_size)
    {
        return;
    }
    uint32_t& chunk = m_dados[pos/32];

    uint32_t mask = 1 << (pos%32);

    if(val)
        chunk |= mask;
    else
        chunk &= ~mask;

}

int Vetor_bool::popcount() const
{
   int soma=0;
   for(unsigned int i=0; i < m_dados.size(); i++)
   {
       soma += __builtin_popcount(m_dados[i]);
   }
   return soma;
}

int Vetor_bool::countZeros() const
{
    return m_size - popcount();
}

int Vetor_bool::comparar(const Vetor_bool& outro) const
{
    if(size() != outro.size())
    {
        std::cout << "Erro: não pode comparar vetores de tamanhos diferentes.\n";
        return 0;
    }

    int contador = 0;
    for(int i=0; i < m_dados.size(); i++)
    {
        contador += __builtin_popcount(m_dados[i] & outro.m_dados[i]);
    }
    return contador;
}
