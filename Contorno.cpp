#include "Contorno.h"
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "Seccao.h"


bool CalcularContorno(Mat& img, std::vector<Point>& saida)
{
    std::vector<std::vector<Point> > contours;
    findContours( img, contours, CV_RETR_EXTERNAL , CV_CHAIN_APPROX_NONE );
    int a=0;
    for(unsigned int i=1; i < contours.size(); i++)
        if(contours[a].size() < contours[i].size()) a = i;
    saida = contours[a];

    return true;
}

void DesenharContorno(Mat& img, std::vector<Point>& saida)
{
    for(unsigned int i=0; i < saida.size(); i++)
    {
        line(img, saida[i], saida[(i+1)%saida.size()], Scalar(255,255,255));
    }
}


Contorno::Contorno(const char* arquivo)
{
    Mat src = imread(arquivo, CV_LOAD_IMAGE_GRAYSCALE);

    if(!src.data)
    {
        std::cout << "Erro: nao foi possivel carregar a imagem \"" << arquivo << "\".\n";
    }
    else
    {
        CalcularContorno(src, m_pontos);
    }
}
Contorno::Contorno(std::vector<Point>& p)
{
    m_pontos = std::move(p);
}


Contorno::~Contorno()
{
    //dtor
}

Contorno Contorno::simplificar(float l)
{
    std::vector<Point> contorno_temporario;
    contorno_temporario = m_pontos;

    if(m_pontos.size() == 0)
        printf("wtf?\n");
    approxPolyDP(contorno_temporario, contorno_temporario, l, true);

    return Contorno(contorno_temporario);
}

Mat Contorno::imagem(int lado)
{
    Mat atom_image = Mat::zeros(lado, lado, CV_8UC3 );
    DesenharContorno(atom_image, m_pontos);
    return atom_image;
}

Seccao Contorno::seccao(int inicio, int quant)
{
    Seccao nova;

    std::vector<Point> dados;

    for(int i=0; i < quant; i++)
    {
        dados.push_back(m_pontos[(i+inicio)%m_pontos.size()]);
    }
    nova.definir(dados);

    return nova;
}

int Contorno::idOf(const Point& p)
{
    for(unsigned int i=0; i < m_pontos.size(); i++)
    {
        if(p == m_pontos[i])
            return i;
    }
    return -1;
}

void Contorno::desenhar(Mat& imagem)
{
    DesenharContorno(imagem, m_pontos);
    desenharVertices(imagem);
}
void Contorno::desenharVertices(Mat& imagem)
{
    for(unsigned int i=0; i < m_pontos.size(); i++)
    {
        circle(imagem, m_pontos[i], 3, Scalar(255), -1);
    }
}

const std::vector<Seccao>& Contorno::getSeccoes()
{
    if(m_seccoes.size() > 0)
        return m_seccoes;
    Contorno simplificado = this->simplificar(15);

    const std::vector<Point>& pontos = simplificado.pontos();

    for(int a=0; a < pontos.size(); a++)
    {
        for(int b=1; b < pontos.size()-1; b++)
        {
            Seccao atual = this->seccao(this->idOf(pontos[a]), this->idOf(pontos[b]));
            if(atual.m_pontos.size() == 0)
                continue;
            if(atual.pontosFora())
                continue; //alguns pontos ficaram fora. ignora esta merda

            m_seccoes.push_back(std::move(atual));
        }
    }

    processarSeccoes();
    return m_seccoes;
}

void Contorno::processarSeccoes()
{
    for(Seccao& atual : m_seccoes)
    {
        atual.processar();
    }
}
