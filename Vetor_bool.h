#ifndef VETOR_BOOL_H
#define VETOR_BOOL_H

#include <cstdint>
#include <cstdio>
#include <vector>
#include <cstring>


class Vetor_bool
{
public:
    Vetor_bool();
    ~Vetor_bool();


    void push_back(bool v);
    inline int size() const
    {
        return m_size;
    }
    void clear();


    uint32_t* data()
    {
        return &m_dados[0];
    }
    std::vector<uint32_t>& data_vector()
    {
        return m_dados;
    }

    void setData(uint32_t* data, int tam)
    {
        m_size = tam;
        m_dados.resize((tam/32)+2);
        memcpy(&m_dados[0], data, (tam/32)+1);
    }
    void setData(std::vector<uint32_t>& dados, int tam)
    {
        m_dados = std::move(dados);
        m_size = tam;
        //TODO: ajeitar m-pos_atual
    }

    bool at(int pos) const;

    void set(unsigned int pos, bool val);

    /*
        define o tamanho do vetor e preenche tudo com zeros (false).
    */
    void resize(int s);


    int popcount() const;
    int countZeros() const;


    /*
        Compara este vetor com outro que tenha exatamente o mesmo tamanho.

        Primeiro faz a operação this AND outro
        Depois conta os bits que são 1
    */
    int comparar(const Vetor_bool& outro) const;

    void dump()
    {
        for(int a=0; a < m_size; a++)
        {
            printf("%i ", at(a));
        }
    }

private:
    void novo_pedaco();

    std::vector<uint32_t> m_dados;

    int m_pos_atual;

    int m_size;


};

#endif // VETOR_BOOL_H
