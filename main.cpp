#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Seccao.h"
#include "Nodo_imagem.h"
#include "ArvoreDeImagens.h"
#include "Contorno.h"


#include "Vetor_bool.h"

using namespace cv;


/** @function main */
int main( int argc, char** argv )
{
    ArvoreDeImagens teste2;

    //teste2.Carregar("banco_de_dados.bin");


    printf("carregando...\n");
    for(int i=0; i < QUANTIDADE_CLASSES; i++)
    {
        std::vector<std::string> arquivos = ArquivosDeTreinoDaClasse(i);
        for(std::string& filename : arquivos)
        {
            printf("carregando(%i/%i) %s\n", i, QUANTIDADE_CLASSES, filename.c_str());
            Contorno temp2(filename.c_str());
            teste2.AdicionarImagem(temp2);
        }
    }

    printf("treinando...\n");
    for(int i=0; i < QUANTIDADE_CLASSES; i++)
    {
        std::vector<std::string> arquivos = ArquivosDeTreinoDaClasse(i);
        for(std::string& filename : arquivos)
        {
            printf("treinando %s\n", filename.c_str());
            Contorno temp2(filename.c_str());
            teste2.Treinar(temp2, i);
        }
    }


    printf("testando....\n");
    int acertos = 0;
    int testes = 0;
    for(int i=0; i < QUANTIDADE_CLASSES; i++)
    {
        std::vector<std::string> arquivos = ArquivosDeTesteDaClasse(i);
        for(std::string& filename : arquivos)
        {
            printf("testando %s ....", filename.c_str());
            Contorno temp2(filename.c_str());

            auto vet = teste2.ClassificarImagem(temp2);

            for(int a=0; a < 1; a++)
            {
                if(vet[a].first == i)
                {
                    printf("Acerto!\n");
                    acertos++;
                    goto PROX_TESTE;
                }
            }
            printf("Erro!\n");

PROX_TESTE:
            testes++;
        }
    }

    float porcentagem = acertos/(float)testes;
    printf("taxa de acerto: %f \n", porcentagem);

    return 0;
}
