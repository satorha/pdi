#ifndef SECCAO_H
#define SECCAO_H

#include <vector>
#include "opencv2/imgproc/imgproc.hpp"
#include "Vetor_bool.h"
#include "config.h"
#include "Nodo_imagem.h"
#include "Contorno.h"

using namespace cv;


class Seccao
{
public:
    ~Seccao();

    //a partir de um conjunto de pontos...
    void definir(std::vector<Point>& p)
    {
        m_pontos = std::move(p);
        m_quantidade_pontos = m_pontos.size();
    }

    /*
        Gera as imagens de todos os niveis
    */
    void processar();

    //só pode ser usado depois que processar() for executado
    const Nodo_imagem& pega_imagem(int nivel) const
    {
       return m_imagem_2d[nivel];
    }


    bool pontosFora()
    {
        return impossivel;
    }
private:
    Seccao();
    friend class Contorno;

    std::vector<Point> m_pontos;
    int m_quantidade_pontos;

    bool impossivel;
    void NormalizaCurva(Point* pontos, int quant, double dimensao_coisa);


    /*
        Os seguintes dados são específicos da implementação atual
    */
    void GeraMatrizCurva();
    std::vector<Nodo_imagem> m_imagem_2d;
};


#endif // SECCAO_H
