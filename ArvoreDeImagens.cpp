#include "ArvoreDeImagens.h"
#include "config.h"
Nodo::Nodo(const Nodo_imagem& i) :
    img(i)
{
    for(int i=0; i < QUANTIDADE_CLASSES; i++)
        frequencia[i] = 0;

    marcador = 0;
}

Nodo::Nodo()
{
    for(int i=0; i < QUANTIDADE_CLASSES; i++)
        frequencia[i] = 0;

    marcador = 0;
}

void Nodo::addNodo(const Seccao& novo)
{
    int proximo_nivel = img.getNivel()+1;

    if(proximo_nivel >= QUANT_CAMADAS)
    {
        return; //ja estamos nonivel maximo
    }

    //printf("novo nodo no nivel %i\n", proximo_nivel);
    Nodo_imagem t(novo.pega_imagem(proximo_nivel));
    filhos.push_back(Nodo(t));

    filhos.back().addNodo(novo); //adiciona ate a profundidade maxima
}

Nodo& Nodo::AchaMelhorFilho(const Seccao& busca)
{
    float passado = 0;

    for(Nodo& atual: filhos)
    {
        float l;
        if((l = atual.img.comparar(busca)) >= LIMIAR)
        {
            return atual.AchaMelhorFilho(busca);
        }
    }
    return *this;
}

Nodo _nodo_padrao()
{
    Vetor_bool tmp;
    tmp.push_back(true);

    return Nodo(Nodo_imagem(tmp, 0));
}

void Nodo::Incrementa(const Seccao& busca, int classe, uint64_t ignorar)
{
    if(marcador != ignorar)
    {
        frequencia[classe]++;
        marcador = ignorar;
    }
    for(Nodo& atual: filhos)
    {
        if(atual.img.comparar(busca) >= LIMIAR)
        {
            atual.Incrementa(busca, classe, ignorar);
        }
    }
}

void Nodo::EscreverNoArquivo(std::fstream& out)
{
    //escreve a imagem
    std::vector<uint32_t>& dados = img.dados_vector();
    uint32_t size_v = dados.size();
    out.write((char*)&size_v, sizeof(uint32_t));
    out.write((char*)&dados[0], sizeof(uint32_t) * dados.size());

    //escreve as frequencias
    out.write((char*)frequencia, sizeof(uint32_t)*QUANTIDADE_CLASSES);

    //escreve a quantidade de filhos
    uint32_t tmp = filhos.size();
    out.write((char*)&tmp, sizeof(uint32_t));

    //escreve os filhos no arquivo
    for(Nodo& atual : filhos)
    {
        atual.EscreverNoArquivo(out);
    }
}

void Nodo::LerDoArquivo(std::fstream& in, int nivel_atual)
{
    //ler a imagem
    int quantidade_bits = RESOLUCAO_CAMADA[nivel_atual]*RESOLUCAO_CAMADA[nivel_atual];

    std::vector<uint32_t> buffer;
    uint32_t s;
    in.read((char*)&s, sizeof(uint32_t));
    buffer.resize(s);
    in.read((char*)&buffer[0], s * sizeof(uint32_t));

    Vetor_bool image_data;
    image_data.setData(buffer, quantidade_bits);
    img = Nodo_imagem(image_data, nivel_atual);

    //ler as frequencias
    in.read((char*)frequencia, sizeof(uint32_t)*QUANTIDADE_CLASSES);

    //ler a quantidade de filhos
    uint32_t tmp = filhos.size();
    in.read((char*)&tmp, sizeof(uint32_t));

    //processa os nodos filhos
    filhos.resize(tmp);
    for(Nodo& atual : filhos)
    {
        atual.LerDoArquivo(in, nivel_atual+1);
    }
}


ArvoreDeImagens::ArvoreDeImagens() :
    raiz(_nodo_padrao())
{
    iteracao_atual = 1;

    quantidade_curvas_cache = -1;
}

ArvoreDeImagens::~ArvoreDeImagens()
{
    //dtor
}


void ArvoreDeImagens::AdicionarCurva(const Seccao& coisa)
{
    Nodo& melhor_lugar = raiz.AchaMelhorFilho(coisa);
    melhor_lugar.addNodo(coisa);
}

void ArvoreDeImagens::Treinar(const Seccao& coisa, int classe)
{
    raiz.Incrementa(coisa, classe, iteracao_atual);
    iteracao_atual++;
}


void ArvoreDeImagens::AdicionarImagem(Contorno& coisa)
{
    const std::vector<Seccao>& seccoes = coisa.getSeccoes();

    for(const Seccao& atual : seccoes)
    {
        AdicionarCurva(atual);
    }
/*
    for(int i=0; i < raiz.filhos.size(); i++)
        printf("raiz %i\n", raiz.filhos[i].filhos.size());*/
}

void ArvoreDeImagens::Treinar(Contorno& coisa, int classe)
{
    const std::vector<Seccao>& seccoes = coisa.getSeccoes();

    for(const Seccao& atual : seccoes)
    {
        Treinar(atual, classe);
    }

}

int ArvoreDeImagens::quantidade_curvas_no_bd()
{
    if(quantidade_curvas_cache != -1)
        return quantidade_curvas_cache;
    int soma=0;
    for(int i=0; i < QUANTIDADE_CLASSES; i++)
        soma += quantidade_da_classe(i);

    quantidade_curvas_cache = soma;

    return soma;
}

int ArvoreDeImagens::quantidade_da_classe(int classe)
{
    return raiz.frequencia[classe];
}

float ArvoreDeImagens::probabilidade_nodo(const Nodo& n, int classe)
{
    if(n.frequencia[classe] == 0)
        return 1/13000.0;

    return n.frequencia[classe]/(float)quantidade_da_classe(classe);
}

double ArvoreDeImagens::calcula_probabilidade(const std::vector<Nodo*>& seccoes, int classe)
{
    double inicio=0;
	for(const Nodo* atual : seccoes)
	{
		double tmp = probabilidade_nodo(*atual, classe);
		inicio += log(tmp);
	}
	inicio += log(quantidade_da_classe(classe)/(double)quantidade_curvas_no_bd());


	return inicio;

}

Nodo& ArvoreDeImagens::AcharMelhorNodo(const Seccao& s)
{
    return raiz.AchaMelhorFilho(s);
}

bool cmp(const std::pair<int, double> &a, const std::pair<int, double> &b)
{
    return a.second < b.second;
}

std::vector<std::pair<int, double> > ArvoreDeImagens::Classificar(std::vector<Nodo*>& nodos)
{
    std::vector<std::pair<int, double> > probs;

    for(int i=0; i < QUANTIDADE_CLASSES; i++)
    {
        double carai = calcula_probabilidade(nodos, i);
        probs.push_back(std::pair<int, double>(i, -carai));
    }

    std::sort(probs.begin(), probs.end(), cmp);

    return probs;
}

std::vector<std::pair<int, double> > ArvoreDeImagens::ClassificarImagem(Contorno& c)
{
    const std::vector<Seccao>& seccoes = c.getSeccoes();

    std::vector<Nodo*> resultados;
    resultados.reserve(seccoes.size());
    for(const Seccao& atual : seccoes)
    {
        Nodo& resultado = AcharMelhorNodo(atual);
        resultados.push_back(&resultado);

    }

    return Classificar(resultados);
}


bool ArvoreDeImagens::Salvar(const char* filename)
{
    std::fstream out(filename, std::fstream::out | std::fstream::binary);

    if(!out.is_open())
        return false;

    raiz.EscreverNoArquivo(out);

    return true;
}


bool ArvoreDeImagens::Carregar(const char* filename)
{
    std::fstream in(filename, std::fstream::in | std::fstream::binary);

    if(!in.is_open())
    {
        printf("arquivo não encontrado.\n");
        return false;
    }


    printf("Carregando dados do arquivo.\n");
    raiz.LerDoArquivo(in);
    printf("Dados carregados.\n");

    return true;

}
