#ifndef CONFIG_P_H
#define CONFIG_P_H

#include <string>
#include <vector>

const int QUANT_CAMADAS = 6;

const int FATOR_NORMALIZANTE = 512;

//a primeira resolucao sempre deve ser 1
const int RESOLUCAO_CAMADA[] = {1, 4, 8, 16, 32, 64};

const float LIMIAR = 0.8;

const int QUANTIDADE_CLASSES = 70;

std::string NomeDaClasse(int i);
std::vector<std::string> ArquivosDeTreinoDaClasse(int i);
std::vector<std::string> ArquivosDeTesteDaClasse(int i);

#endif
