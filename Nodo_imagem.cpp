#include "Nodo_imagem.h"
#include "config.h"
#include "Seccao.h"

Nodo_imagem::Nodo_imagem(Vetor_bool& imagem, int ordem)
{
    m_dados = imagem;
    m_ordem = ordem;
}
Nodo_imagem::Nodo_imagem()
{
    m_ordem = -1;
}


Nodo_imagem::~Nodo_imagem()
{
    //dtor
}

void Nodo_imagem::desenhar(Mat& out, int tamanho) const
{
    float pulo = tamanho/(float)RESOLUCAO_CAMADA[m_ordem];

//    m_dados.dump();
    int desenhados = 0;
    printf("%i\n", RESOLUCAO_CAMADA[m_ordem]);
    int atual=0;
    for(int i=0; i < RESOLUCAO_CAMADA[m_ordem]; i++)
    {
        for(int a=0; a < RESOLUCAO_CAMADA[m_ordem]; a++)
        {
            if(m_dados.at(atual++)) rectangle(out, Rect(a*pulo, i*pulo, pulo, pulo), Scalar(0, 0, 255), -1);
            else rectangle(out, Rect(a*pulo, i*pulo, pulo, pulo), Scalar(255), -1);
            desenhados++;
        }
    }
    printf("%i\n", desenhados);
}

float Nodo_imagem::comparar(const Seccao& s) const
{
    return comparar(s.pega_imagem(m_ordem));
}

float Nodo_imagem::comparar(const Nodo_imagem& n) const
{
    int iguais = m_dados.comparar(n.m_dados);

    double c = max(m_dados.popcount(), n.m_dados.popcount());

    if(c == 0) c = 1;

	return iguais/(double)c;
}
