#ifndef NODO_IMAGEM_H
#define NODO_IMAGEM_H
#include "opencv2/imgproc/imgproc.hpp"
#include "Vetor_bool.h"


using namespace cv;
class Seccao;

class Nodo_imagem
{
public:
    explicit Nodo_imagem(Vetor_bool& imagem, int ordem);
    explicit Nodo_imagem();
    ~Nodo_imagem();


    //desenha pra um bitmap da opencv
    void desenhar(Mat& out, int tamanho) const;

    //compara com outro e retorna um número entre 0 e 1
    float comparar(const Nodo_imagem& outro) const;
    float comparar(const Seccao& s) const; //automaticamente pega a imagem de resolucao certa

    int getNivel() const
    {
        return m_ordem;
    }

    int deb() const
    {
        return m_dados.size();
    }

    uint32_t* dados()
    {
        return m_dados.data();
    }
    std::vector<uint32_t>& dados_vector()
    {
        return m_dados.data_vector();
    }
private:
    int m_ordem;
    Vetor_bool m_dados;
};

#endif // NODO_IMAGEM_H
