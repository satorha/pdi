#ifndef ARVOREDEIMAGENS_H
#define ARVOREDEIMAGENS_H

#include "Nodo_imagem.h"
#include "Seccao.h"
#include <cstdint>
#include <fstream>

class Nodo
{
public:
    //adiciona um novo filho a este nodo
    void addNodo(const Seccao& novo);

    //acha o filho deste nodo que possui maior grau de semalhança com a secção especificada
    Nodo& AchaMelhorFilho(const Seccao& busca);

    /*
        Percorre a árvore da mesma forma que AchaMelhorFilho() faria, somando um em cada um na frequencia da classe especificada nos nodos que fizerem parte do caminho

        todos os nodos em que marcador for igual a ignorar não serão incrementados.
        Ao final da função, ignorar é copiado em marcador
    */
    void Incrementa(const Seccao& busca, int classe, uint64_t ignorar);



    void EscreverNoArquivo(std::fstream& out);

    void LerDoArquivo(std::fstream& in, int nivel_atual=0);

    explicit Nodo();
private:
    explicit Nodo(const Nodo_imagem& img);

    friend class ArvoreDeImagens;
    friend Nodo _nodo_padrao();
    Nodo_imagem img;
    std::vector<Nodo> filhos;

    uint32_t frequencia[QUANTIDADE_CLASSES];

    uint64_t marcador;
};

class ArvoreDeImagens
{
public:
    ArvoreDeImagens();
    ~ArvoreDeImagens();

    void AdicionarImagem(Contorno& c);
    void Treinar(Contorno& c, int classe); //insere no treino

    std::vector<std::pair<int, double> > ClassificarImagem(Contorno& c);

    int quantidade_curvas_no_bd();
    int quantidade_da_classe(int classe);

    bool Salvar(const char* str);
    bool Carregar(const char* str);

    //acha o nodo que mais se parece com uma certa secção.
    Nodo& AcharMelhorNodo(const Seccao& s);

    //classifica um consjunto de nodos
    std::vector<std::pair<int, double> > Classificar(std::vector<Nodo*>& nodos);

private:
    Nodo raiz;

    void AdicionarCurva(const Seccao& coisa);
    void Treinar(const Seccao& coisa, int classe);

    float probabilidade_nodo(const Nodo& n, int classe);

    double calcula_probabilidade(const std::vector<Nodo*>& seccoes, int classe);

    uint64_t iteracao_atual;

    int quantidade_curvas_cache;
};

#endif // ARVOREDEIMAGENS_H
