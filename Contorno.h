#ifndef CONTORNO_H
#define CONTORNO_H

#include <vector>
#include <algorithm>
#include "opencv2/imgproc/imgproc.hpp"

class Seccao;

using namespace cv;

class Contorno
{
public:
    explicit Contorno(const char* arquivo);

    //obs: o vetor passado como argumento será alterado
    explicit Contorno(std::vector<Point>& p);
    ~Contorno();

    Contorno simplificar(float l);

    Mat imagem(int lado);

    const std::vector<Point>& pontos()
    {
        return m_pontos;
    }

    Seccao seccao(int inicio, int fim);

    //retorna qual posição no vetor possui um ponto igual ao especificado.
    //Caso não exista, retorna -1
    int idOf(const Point& p);


    void desenhar(Mat& imagem);
    void desenharVertices(Mat& imagem);

    const std::vector<Seccao>& getSeccoes();
private:
    std::vector<Point> m_pontos;

    std::vector<Seccao> m_seccoes; //armazenas todas as seccoes

    void processarSeccoes();

};

#endif // CONTORNO_H
