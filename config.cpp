#include "config.h"
#include <fstream>

std::string nomes[] =
{
    "apple",
    "bat",
    "beetle",
    "bell",
    "bird",
    "Bone",
    "bottle",
    "brick",
    "butterfly",
    "camel",
    "car",
    "carriage",
    "cattle",
    "cellular_phone",
    "chicken",
    "children",
    "chopper",
    "classic",
    "Comma",
    "crown",
    "cup",
    "deer",
    "device0",
    "device1",
    "device2",
    "device3",
    "device4",
    "device5",
    "device6",
    "device7",
    "device8",
    "device9",
    "dog",
    "elephant",
    "face",
    "fish",
    "flatfish",
    "fly",
    "fork",
    "fountain",
    "frog",
    "Glas",
    "guitar",
    "hammer",
    "hat",
    "HCircle",
    "Heart",
    "horse",
    "horseshoe",
    "jar",
    "key",
    "lizzard",
    "lmfish",
    "Misk",
    "octopus",
    "pencil",
    "personal_car",
    "pocket",
    "rat",
    "ray",
    "sea_snake",
    "shoe",
    "spoon",
    "spring",
    "stef",
    "teddy",
    "tree",
    "truck",
    "turtle",
    "watch",
};

std::string NomeDaClasse(int i)
{
    return nomes[i];
}

std::vector<std::string> ArquivosDeTreinoDaClasse(int classeid)
{
    std::vector<std::string> temp;

    for(int i=1; i <= 10; i++)
    {
        std::string atual = NomeDaClasse(classeid) + "-" + std::to_string(i) + ".png";

        if(!std::ifstream(atual.c_str()).good())
        {
            atual = NomeDaClasse(classeid) + "-0" + std::to_string(i) + ".png";
        }


        temp.push_back(atual);
    }

    return temp;
}

std::vector<std::string> ArquivosDeTesteDaClasse(int classeid)
{
    std::vector<std::string> temp;

    for(int i=11; i <= 20; i++)
    {
        std::string atual = NomeDaClasse(classeid) + "-" + std::to_string(i) + ".png";
        temp.push_back(atual);
    }

    return temp;
}
